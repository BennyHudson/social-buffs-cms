<?php
	function wedo_posts() {

			register_post_type(
				'case-studies',
				[
          'public' => true,
          'has_archive' => true,
          'label' => 'Case Studies',
          'show_in_graphql' => true,
          'supports'		=> array(
            'title',
            'editor',
            'excerpt',
            'revisions',
            'thumbnail'
          ),
          'graphql_single_name' => 'caseStudy',
          'graphql_plural_name' => 'caseStudies'
        ]
			);

      register_post_type(
				'careers',
				[
          'public' => true,
          'has_archive' => true,
          'label' => 'Careers',
          'show_in_graphql' => true,
          'supports'		=> array(
            'title',
            'editor',
            'excerpt',
            'revisions',
            'thumbnail'
          ),
          'graphql_single_name' => 'career',
          'graphql_plural_name' => 'careers'
        ]
			);

    }
?>
