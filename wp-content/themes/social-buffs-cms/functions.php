<?php

	add_action( 'after_setup_theme', 'wedo_theme_setup' );
	function wedo_theme_setup() {
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_filter( 'wp_mail_from_name', 'new_mail_from_name' );
		add_action( 'login_enqueue_scripts', 'my_login_logo' );
    add_action( 'init', 'register_my_menus' );
		add_action( 'login_head', 'add_favicon' );
		add_action( 'admin_head', 'add_favicon' );
		add_action( 'init', 'wedo_posts' );
		add_action( 'init', 'flush_rewrite_rules' );
		add_action( 'wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);
		add_filter( 'wpseo_metabox_prio', function() { return 'low';});
		add_image_size( 'post-thumb', 400, 400, true );
		add_image_size( 'post-feature', 2000, 600, true );
    add_image_size( 'logo', 300, 300, false );
	}

	get_template_part( 'functions/include', 'favicons' );
	get_template_part( 'functions/include', 'cpts' );
	get_template_part( 'functions/include', 'users' );
	get_template_part( 'functions/include', 'email' );
	get_template_part( 'functions/include', 'footer' );
  get_template_part( 'functions/include', 'menus' );

	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'index_rel_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );

	add_filter( 'acf/settings/google_api_key', function () {
	    return 'AIzaSyBR5bX6m_CEPwitun65XjrFWYZVRtzqADA';
	});

?>
